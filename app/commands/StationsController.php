<?php

namespace app\commands;

use app\models\Stations;
use app\utils\GpnClient;
use yii\console\Controller;
use yii\db\Expression;

/**
 * Class StationsController
 * @package app\commands
 */
class StationsController extends Controller
{

    public function actionIndex(): void
    {
        $gpn = new GpnClient();

        $gpn->each(function (\ArrayObject $station): void {

            /**
             * Механизьм сохранения не реализован до конца
             */
            $model = new Stations();
            $model->name = $station->offsetGet('name');
            $model->location = new Expression(
                "PointFromText('POINT(" . implode(' ', $station->offsetGet('location')) . ")')"
            );

            /**
             * yii не может в многие из многих
             */
            /*$model->fuels = $station->offsetGet('fuels');
            $model->services = $station->offsetGet('services');*/

            $model->save();
            var_dump($station->offsetGet('name'));

        });

    }

}
