<?php

namespace app\utils;

use yii\httpclient\{Client, Response, Exception};
use PHPHtmlParser\Dom;

/**
 * Class GpnClient
 * @package app\utils
 */
class GpnClient
{
    protected const URL = 'https://www.gpnbonus.ru/our_azs/';

    protected const STATION_CONTAINER = '.azs_container';
    protected const STATION_NAME = '[id^="azs_address_"]';
    protected const STATION_INFO = '[id^="azs_other_info_"] .pt10';

    protected const FUEL_NAME = '.DinPro';
    protected const FUEL_PRICE = '[style^=";color:"]';

    /**
     * @param callable $callback
     * @throws Exception
     */
    public function each(callable $callback): void
    {
        $this->getContainer()->each(function (Dom\HtmlNode &$station) use ($callback): void {
            $info = &$this->getInfo($station);
            $callback($this->getEntity([
                'name' => $this->getName($station),
                'location' => $this->getLocation($info[0]),
                'fuels' => $this->getFuels($info[1]),
                'services' => $this->getServices($info[2])
            ]));
        });
    }

    /**
     * @param Dom\HtmlNode $info
     * @return array
     */
    protected function getServices(Dom\HtmlNode $info): array
    {
        $services = [];
        $info->find('img')->each(function (Dom\HtmlNode $service) use (&$services): void {
            $services[] = $this->getEntity([
                'name' => $this->getServiceName($service)
            ]);
        });
        return $services;
    }

    /**
     * @param Dom\HtmlNode $service
     * @return string
     */
    protected function getServiceName(Dom\HtmlNode $service): string
    {
        return trim($service->getAttribute('title'));
    }

    /**
     * @param Dom\HtmlNode $info
     * @return array
     */
    protected function getFuels(Dom\HtmlNode $info): array
    {
        $fuels = [];
        $info->find('div')->each(function (Dom\HtmlNode $fuel) use (&$fuels): void {
            $fuels[] = $this->getEntity([
                'name' => $this->getFuelName($fuel),
                'price' => $this->getFuelPrice($fuel)
            ]);
        });
        return $fuels;
    }

    /**
     * @param Dom\HtmlNode $fuel
     * @return int|null
     */
    protected function getFuelPrice(Dom\HtmlNode $fuel): ?int
    {
        $pattern = '#(\d+(?:[\.,]\d+)?)#';
        $price = $fuel->find($this::FUEL_PRICE)->firstChild()->text();
        if (!preg_match($pattern, $price, $out)) {
            return null;
        }
        return $out[0] * 100;
    }

    /**
     * @param Dom\HtmlNode $fuel
     * @return string
     */
    protected function getFuelName(Dom\HtmlNode $fuel): string
    {
        return trim($fuel->find($this::FUEL_NAME)->firstChild()->text());
    }

    /**
     * @param Dom\HtmlNode $info
     * @return null|array
     */
    protected function getLocation(Dom\HtmlNode $info): ?array
    {
        $pattern = '#\> (\d+(?:[\.,]\d+)?) (\d+(?:[\.,]\d+)?)#';
        if (!preg_match($pattern, $info->outerHtml(), $out)) {
            return null;
        }
        return [(float)$out[1], (float)$out[2]];
    }

    /**
     * @param Dom\HtmlNode $station
     * @return string
     */
    protected function getName(Dom\HtmlNode $station): string
    {
        return trim($station->find($this::STATION_NAME)->firstChild()->text());
    }

    /**
     * @return Dom\Collection
     * @throws Exception
     */
    protected function getContainer(): Dom\Collection
    {
        return $this->getDom()->find($this::STATION_CONTAINER);
    }

    /**
     * @param Dom\HtmlNode $station
     * @return Dom\Collection
     */
    protected function getInfo(Dom\HtmlNode $station): Dom\Collection
    {
        return $station->find($this::STATION_INFO);
    }

    /**
     * @return Dom
     * @throws Exception
     */
    protected function getDom(): Dom
    {
        $dom = new Dom;
        $dom->loadStr($this->getResponse());
        return $dom;
    }

    /**
     * @return Response
     * @throws Exception
     */
    protected function getResponse(): Response
    {
        return $this->getClient()->get($this::URL)->send();
    }

    /**
     * @return \yii\httpclient\Client
     */
    protected function getClient(): Client
    {
        return new Client();
    }

    /**
     * @param array $values
     * @return \ArrayObject
     */
    public function getEntity(array $values): \ArrayObject
    {
        return new \ArrayObject($values);
    }
}