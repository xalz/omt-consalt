<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Stations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Stations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="stations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'location',
            'fuels' => [
                'attribute' => 'fuels',
                'label' => 'Топливо',
                'value' => function(\app\models\Stations $model) {
                    return \app\widgets\Many::widget([
                        'items' => $model->fuels,
                        'column' => 'name'
                    ]);
                },
            ],
            'services' => [
                'attribute' => 'services',
                'label' => 'Услуги',
                'value' => function(\app\models\Stations $model) {
                    return \app\widgets\Many::widget([
                        'items' => $model->services,
                        'column' => 'name'
                    ]);
                },
            ],
        ]
    ]) ?>

</div>
