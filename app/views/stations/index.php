<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stations-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'location',
            'fuels' => [
                'attribute' => 'fuels',
                'label' => 'Топливо',
                'value' => function(\app\models\Stations $model) {
                    return \app\widgets\Many::widget([
                        'items' => $model->fuels,
                        'column' => 'name'
                    ]);
                },
            ],
            'services' => [
                'attribute' => 'services',
                'label' => 'Услуги',
                'value' => function(\app\models\Stations $model) {
                    return \app\widgets\Many::widget([
                        'items' => $model->services,
                        'column' => 'name'
                    ]);
                },
            ]
        ],
    ]); ?>
</div>
