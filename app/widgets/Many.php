<?php
namespace app\widgets;

use yii\base\Widget;

/**
 * Class Many
 * @package app\widgets
 */
class Many extends Widget
{
    public $items = [];
    public $column;

    /**
     * @return string
     */
    public function run(): string
    {
        return implode(', ', array_column($this->items, $this->column));
    }
}