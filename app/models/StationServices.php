<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $service_id
 * @property int $station_id
 */
class StationServices extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'station_services';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['service_id', 'station_id'], 'required'],
            [['service_id', 'station_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'service_id' => 'Service ID',
            'station_id' => 'Station ID',
        ];
    }
}
