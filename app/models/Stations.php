<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $name
 * @property string $location
 * @property Fuels $fuels
 * @property Services $services
 */
class Stations extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'stations';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name', 'location'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'location' => 'GPS',
        ];
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getFuels(): ActiveQuery
    {
        return $this
            ->hasMany(Fuels::class, ['id' => 'fuel_id'])
            ->viaTable(StationFuels::tableName(), ['station_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getServices(): ActiveQuery
    {
        return $this
            ->hasMany(Services::class, ['id' => 'service_id'])
            ->viaTable(StationServices::tableName(), ['station_id' => 'id']);
    }

}
