<?php

namespace app\models;

/**
 * @property int $id
 * @property string $name
 */
class Fuels extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'fuels';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['price'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['price'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Название топлива',
        ];
    }
}
