<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $fuel_id
 * @property int $station_id
 */
class StationFuels extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'station_fuels';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['fuel_id', 'station_id'], 'required'],
            [['fuel_id', 'station_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'fuel_id' => 'Fuel ID',
            'station_id' => 'Station ID',
        ];
    }
}
