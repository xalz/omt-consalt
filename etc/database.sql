CREATE TABLE `test`.`stations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `location` point NOT NULL,
  PRIMARY KEY (`id`, `name`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `test`.`fuels` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `price` int(11) NOT NULL,
 PRIMARY KEY (`id`, `name`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `test`.`station_fuels` (
 `fuel_id` int(11) NOT NULL,
 `station_id` int(11) NOT NULL,
 PRIMARY KEY (`fuel_id`, `station_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `test`.`services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`, `name`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `test`.`station_services` (
  `service_id` int(11) NOT NULL,
  `station_id` int(11) NOT NULL,
  PRIMARY KEY (`service_id`, `station_id`)
) DEFAULT CHARSET=utf8;